import * as React from 'react';
import logo from './stack.svg';
import { BrowserRouter, Switch, Route } from 'react-router-dom';
import { Config } from './Config/config';
import useFetchSWR from './Hook/useFetchSWR';
import Warehouse from './Pages/Warehouse';
import WarehouseDetail from './Pages/WarehouseDetail';

import Box from '@mui/material/Box';
import Drawer from '@mui/material/Drawer';
import List from '@mui/material/List';
import ListItem from '@mui/material/ListItem';
import ListItemIcon from '@mui/material/ListItemIcon';
import ListItemText from '@mui/material/ListItemText';
import ListSubheader from '@mui/material/ListSubheader';
import TuneOutlinedIcon from '@mui/icons-material/TuneOutlined';

function App(props) {

  const { data } = new useFetchSWR(Config.apiUrl);
  const drawerWidth = 240;

  const drawer = (
    <>
      <List sx={{ bgcolor: "#232F3E" }}>
        <ListItem>
          <ListItemIcon>
            <img width='30' src={logo} alt="" />
          </ListItemIcon>
          <ListItemText
            sx={{ color: '#FFFFFF' }}
            primary="Test"
          />
        </ListItem>
      </List>
      <List
        sx={{ width: '100%', maxWidth: 360, bgcolor: '#1B2430' }}
        subheader={
          <ListSubheader sx={{ bgcolor: "#1B2430" }} component="div" id="nested-list-subheader">
            <ListItemText sx={{ color: "#FFFFFF", py: 1 }} primary='Test Menu' />
          </ListSubheader>
        }>
        <ListItem sx={{ bgcolor: "#1C1C1C" }} button>
          <ListItemIcon>
            <TuneOutlinedIcon sx={{ color: "#FFFFFF" }} />
          </ListItemIcon>
          <ListItemText sx={{ color: "#FFFFFF" }} primary='Test SM Warehouse' />
        </ListItem>
      </List>
    </>
  );

  return (
    <Box sx={{ display: 'flex' }}>
      <Box
        component="nav"
        sx={{ width: { sm: drawerWidth }, flexShrink: { sm: 0 } }}
      >
        <Drawer
          variant="permanent"
          sx={{
            display: { xs: 'none', sm: 'block' },
            '& .MuiDrawer-paper': {
              boxSizing: 'border-box',
              width: drawerWidth,
              bgcolor: "#1B2430"
            },
          }}
          ModalProps={{
            keepMounted: true,
          }}
          open
        >
          {drawer}
        </Drawer>
      </Box>
      <Box component="main" sx={{ flexGrow: 1, p: 4 }}>
        <BrowserRouter>
          <Switch>
            <Route path={'/'} exact>
              < Warehouse />
            </Route>
            <Route
              exact
              path="/warehouse-detail/:id"
              render={({ match }) => (
                <WarehouseDetail item={data !== undefined ? data.find((item) => String(item.WarehouseID) === String(match.params.id)) : undefined} />
              )}
            />
          </Switch>
        </BrowserRouter>
      </Box >
    </Box >
  );
}

export default App;
