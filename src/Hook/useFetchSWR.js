import useSWR from 'swr';

function useFetchSWR(url) {
    const fetcher = (url) => fetch(url).then((res) => res.json());
    const { data, error } = useSWR(url, fetcher)
    return { data, error }
}

export default useFetchSWR;