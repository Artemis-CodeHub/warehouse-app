import React from 'react';

import Typography from '@mui/material/Typography';
import { DataGrid } from '@mui/x-data-grid';

export default function TableView(data) {

    const columns = [
        {
            field: 'id',
            headerName: 'Location ID',
            width: 150,
        },
        {
            field: 'pick_priority',
            headerName: 'Pick Priority',
            width: 150,
            editable: true,
        },
        {
            field: 'active',
            headerName: 'Active',
            width: 150,
            editable: true,
        },
        {
            field: 'description',
            headerName: 'Description',
            width: 200,
            editable: true,
        },
        {
            field: 'sales_allowed',
            headerName: 'Sales Allowed',
            width: 220,
            editable: true,
        },
        {
            field: 'transfers_allowed',
            headerName: 'Transfers Allowed',
            width: 220,
            editable: true,
        }
    ];

    const rows = (data.data.Locations !== undefined) &&
        data.data.Locations.map((item) => ({
            id: item.LocationID, pick_priority: item.PickPriority, active: item.Active, description: item.Description, sales_allowed: item.SalesAllowed, transfers_allowed: item.TransfersAllowed
        }));

    return (
        <>
            {data.data.Locations[0] !== undefined ?
                <div style={{ height: 400, width: '100%' }}>
                    <DataGrid
                        rows={rows}
                        columns={columns}
                        pageSize={5}
                        rowsPerPageOptions={[5]}
                        checkboxSelection
                        disableSelectionOnClick
                        getRowId={(row) => row.id}

                    />
                </div> :
                <Typography variant="h5" sx={{ p: 3 }}>
                    Loading...
                </Typography>}
        </>
    )
}
