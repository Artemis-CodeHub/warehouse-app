import React from 'react';
import useFetchSWR from '../Hook/useFetchSWR.js';
import { Config } from '../Config/config'

import { DataGrid } from '@mui/x-data-grid';
import Typography from '@mui/material/Typography';

export default function TableView() {

    const { data, error } = new useFetchSWR(Config.apiUrl);

    const columns = [
        {
            field: 'id',
            headerName: 'ID',
            width: 150,
            renderCell: (params) => {
                return (
                    <>
                        <a href={`/warehouse-detail/${params.row.id}`}>{params.row.id}</a>
                    </>
                );
            },
        },
        {
            field: 'branch',
            headerName: 'Branch',
            width: 150,
            editable: true,
        },
        {
            field: 'active',
            headerName: 'Active',
            width: 150,
            editable: true,
        },
        {
            field: 'description',
            headerName: 'Description',
            type: 'number',
            width: 200,
            editable: true,
        },
        {
            field: 'last_sync',
            headerName: 'Last Sync',
            type: 'number',
            width: 220,
            editable: true,
        }
    ];

    let datafiltered = [];
    if (data !== undefined) {
        for (let i = 0; i < data.length; i++) {
            if (data[i].WarehouseID === "RETAIL") {
                datafiltered.push(data[i]);
            } if (data[i].WarehouseID === "VA-RETAIL") {
                datafiltered.push(data[i]);
            }
        }
    }

    const rows = (datafiltered !== undefined) &&
        datafiltered.map((dat) => (
            {
                id: dat.WarehouseID, branch: dat.Branch, active: dat.Active, description: dat.Description, last_sync: dat.LastSync
            }
        ));

    return (
        <>
            {data !== undefined ? <div style={{ height: 400, width: '100%' }}>
                <DataGrid
                    rows={rows}
                    columns={columns}
                    pageSize={5}
                    rowsPerPageOptions={[5]}
                    checkboxSelection
                    disableSelectionOnClick
                />
            </div> : error ?
                <Typography variant="h5" sx={{ p: 3 }}>
                    Failed to Fetch Data...
                </Typography> :
                <Typography variant="h5" sx={{ p: 3 }}>
                    Loading...
                </Typography>}
        </>
    )
}
