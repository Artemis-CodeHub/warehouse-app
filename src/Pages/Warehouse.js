import React from 'react';
import TableView from '../Components/TableView.js';

import Typography from '@mui/material/Typography';
import Breadcrumbs from '@mui/material/Breadcrumbs';
import Link from '@mui/material/Link';
import Box from '@mui/material/Box';

export default function Warehouse() {

    function handleClick(event) {
        event.preventDefault();
        console.info('You clicked a breadcrumb.');
    }

    return (
        <Box>
            <div role="presentation" onClick={handleClick}>
                <Breadcrumbs aria-label="breadcrumb">
                    <Link underline="hover" color="inherit" href="/">
                        Home
                    </Link>
                    <Typography color="text.primary">Warehouse</Typography>
                </Breadcrumbs>
            </div>

            <Typography variant="h5" sx={{ py: 4 }}>
                Warehouse
            </Typography>

            <TableView />
        </Box>
    )
}
