import React from 'react';
import TableViewID from '../Components/TableViewID';
import Typography from '@mui/material/Typography';
import Breadcrumbs from '@mui/material/Breadcrumbs';
import { Link } from 'react-router-dom';
import Box from '@mui/material/Box';

export default function WarehouseDetail({ item }) {

    function handleClick(event) {
        event.preventDefault();
        console.info('You clicked a breadcrumb.');
    }

    return (
        <Box>
            {item === undefined ? <div>Getting Data...</div> :
                <div>
                    <div role="presentation" onClick={handleClick}>
                        <Breadcrumbs aria-label="breadcrumb">
                            <Link underline="hover" color="inherit" to="/">
                                Home
                            </Link>
                            <Link
                                underline="hover"
                                color="inherit"
                                to="/"
                            >
                                Warehouse
                            </Link>
                            <Typography color="text.primary">Detail Location Warehouse - {item.WarehouseID}</Typography>
                        </Breadcrumbs>
                    </div>

                    <Typography variant="h5" sx={{ py: 4 }}>
                        Detail Location Warehouse - Retail {item.WarehouseID}
                    </Typography>

                    <TableViewID data={item} />
                </div>
            }
        </Box>
    )
}
