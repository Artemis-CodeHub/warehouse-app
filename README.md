# Getting Started with Warehouse App

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Available Scripts

In the project directory, you can run:

### `npm i`

For installing all dependencies that will needed.

### `npm run start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

## Screenshot 

Image Warehouse App [Section home with table only displayed RETAIL and VA-RETAIL](https://i.ibb.co/b61kLB5/React-App-Home.png).

Image Warehouse App [Section detail with table displayed all of the data from RETAIL](https://i.ibb.co/b61kLB5/React-App-Home.png).
